package com.steven.frio.numberconverter;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class DecimalFragment extends Fragment {
    View v;

    public DecimalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_decimal, container, false);


        final EditText editDecimal = (EditText) v.findViewById(R.id.editDecimal);
        final EditText editBinary = (EditText) v.findViewById(R.id.editBinary);
        final EditText editOctal = (EditText) v.findViewById(R.id.editOctal);
        final EditText editHexadecimal = (EditText) v.findViewById(R.id.editHexadecimal);

        Button btn_1 = (Button) v.findViewById(R.id.btn_1);
        Button btn_2 = (Button) v.findViewById(R.id.btn_2);
        Button btn_3 = (Button) v.findViewById(R.id.btn_3);
        Button btn_4 = (Button) v.findViewById(R.id.btn_4);
        Button btn_5 = (Button) v.findViewById(R.id.btn_5);
        Button btn_6 = (Button) v.findViewById(R.id.btn_6);
        Button btn_7 = (Button) v.findViewById(R.id.btn_7);
        Button btn_8 = (Button) v.findViewById(R.id.btn_8);
        Button btn_9 = (Button) v.findViewById(R.id.btn_9);
        Button btn_0 = (Button) v.findViewById(R.id.btn_0);
        Button btn_go = (Button) v.findViewById(R.id.btn_go);
        Button btn_clr = (Button) v.findViewById(R.id.btn_clr);

        View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button) v;


                switch (btn.getId()) {
                    case R.id.btn_1:
                        editDecimal.setText(editDecimal.getText() + "1");
                        break;

                    case R.id.btn_2:
                        editDecimal.setText(editDecimal.getText() + "2");
                        break;

                    case R.id.btn_3:
                        editDecimal.setText(editDecimal.getText() + "3");
                        break;

                    case R.id.btn_4:
                        editDecimal.setText(editDecimal.getText() + "4");
                        break;

                    case R.id.btn_5:
                        editDecimal.setText(editDecimal.getText() + "5");
                        break;

                    case R.id.btn_6:
                        editDecimal.setText(editDecimal.getText() + "6");
                        break;

                    case R.id.btn_7:
                        editDecimal.setText(editDecimal.getText() + "7");
                        break;

                    case R.id.btn_8:
                        editDecimal.setText(editDecimal.getText() + "8");
                        break;

                    case R.id.btn_9:
                        editDecimal.setText(editDecimal.getText() + "9");
                        break;

                    case R.id.btn_0:
                        editDecimal.setText(editDecimal.getText() + "0");
                        break;

                    case R.id.btn_clr:
                        editDecimal.setText("");
                        editBinary.setText("");
                        editOctal.setText("");
                        editHexadecimal.setText("");
                        break;

                    case R.id.btn_go:
                        try {
                            int decimal = Integer.parseInt(editDecimal.getText().toString());


                            editBinary.setText(Integer.toString(decimal, 2));

                            editOctal.setText(Integer.toString(decimal, 8));

                            editHexadecimal.setText(Integer.toString(decimal, 16));

                        } catch (NumberFormatException e) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_decimal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            AlertDialog alertalert = builder.create();
                            alertalert.show();

                        }


                        break;


                }
            }
        };

        btn_1.setOnClickListener(onClick);
        btn_2.setOnClickListener(onClick);
        btn_3.setOnClickListener(onClick);
        btn_4.setOnClickListener(onClick);
        btn_5.setOnClickListener(onClick);
        btn_6.setOnClickListener(onClick);
        btn_7.setOnClickListener(onClick);
        btn_8.setOnClickListener(onClick);
        btn_9.setOnClickListener(onClick);
        btn_0.setOnClickListener(onClick);
        btn_go.setOnClickListener(onClick);
        btn_clr.setOnClickListener(onClick);


        return v;
    }

}
