package com.steven.frio.numberconverter;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class OctalFragment extends Fragment {
    View v;

    public OctalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_octal, container, false);

        final EditText editOctal = (EditText) v.findViewById(R.id.editOctal);
        final EditText editBinary = (EditText) v.findViewById(R.id.editBinary);
        final EditText editDecimal = (EditText) v.findViewById(R.id.editDecimal);
        final EditText editHexadecimal = (EditText) v.findViewById(R.id.editHexadecimal);

        Button btn_1 = (Button) v.findViewById(R.id.btn_1);
        Button btn_2 = (Button) v.findViewById(R.id.btn_2);
        Button btn_3 = (Button) v.findViewById(R.id.btn_3);
        Button btn_4 = (Button) v.findViewById(R.id.btn_4);
        Button btn_5 = (Button) v.findViewById(R.id.btn_5);
        Button btn_6 = (Button) v.findViewById(R.id.btn_6);
        Button btn_7 = (Button) v.findViewById(R.id.btn_7);
        Button btn_0 = (Button) v.findViewById(R.id.btn_0);
        Button btn_go = (Button) v.findViewById(R.id.btn_go);
        Button btn_clr = (Button) v.findViewById(R.id.btn_clr);

        View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button) v;

                switch (btn.getId()) {
                    case R.id.btn_1:
                        editOctal.setText(editOctal.getText() + "1");
                        break;
                    case R.id.btn_2:
                        editOctal.setText(editOctal.getText() + "2");
                        break;
                    case R.id.btn_3:
                        editOctal.setText(editOctal.getText() + "3");
                        break;
                    case R.id.btn_4:
                        editOctal.setText(editOctal.getText() + "4");
                        break;
                    case R.id.btn_5:
                        editOctal.setText(editOctal.getText() + "5");
                        break;
                    case R.id.btn_6:
                        editOctal.setText(editOctal.getText() + "6");
                        break;
                    case R.id.btn_7:
                        editOctal.setText(editOctal.getText() + "7");
                        break;
                    case R.id.btn_0:
                        editOctal.setText(editOctal.getText() + "0");
                        break;
                    case R.id.btn_clr:
                        editOctal.setText("");
                        editBinary.setText("");
                        editDecimal.setText("");
                        editHexadecimal.setText("");
                        break;
                    case R.id.btn_go:
                        try {
                            int octal = Integer.parseInt(editOctal.getText().toString(), 8);

                            editBinary.setText(Integer.toString(octal, 2));

                            editDecimal.setText(Integer.toString(octal, 10));

                            editHexadecimal.setText(Integer.toString(octal, 16));
                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_octal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;
                }
            }
        };

        btn_1.setOnClickListener(onClick);
        btn_2.setOnClickListener(onClick);
        btn_3.setOnClickListener(onClick);
        btn_4.setOnClickListener(onClick);
        btn_5.setOnClickListener(onClick);
        btn_6.setOnClickListener(onClick);
        btn_7.setOnClickListener(onClick);
        btn_0.setOnClickListener(onClick);
        btn_go.setOnClickListener(onClick);
        btn_clr.setOnClickListener(onClick);


        return v;
    }

}
