package com.steven.frio.numberconverter;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalculatorFragment extends Fragment {
    View v;


    int decimal1, binary1, octal1, hexa1,
            decimal2, binary2, octal2, hexa2,
            sum, difference;


    public CalculatorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_calculator, container, false);

        final EditText input1_decimal = (EditText) v.findViewById(R.id.input1_decimal);
        final EditText input1_binary = (EditText) v.findViewById(R.id.input1_binary);
        final EditText input1_octal = (EditText) v.findViewById(R.id.input1_octal);
        final EditText input1_hexa = (EditText) v.findViewById(R.id.input1_hexa);


        final EditText input2_decimal = (EditText) v.findViewById(R.id.input2_decimal);
        final EditText input2_binary = (EditText) v.findViewById(R.id.input2_binary);
        final EditText input2_octal = (EditText) v.findViewById(R.id.input2_octal);
        final EditText input2_hexa = (EditText) v.findViewById(R.id.input2_hexa);


        final EditText outputBinary = (EditText) v.findViewById(R.id.outputBinary);
        final EditText outputOctal = (EditText) v.findViewById(R.id.outputOctal);
        final EditText outputDecimal = (EditText) v.findViewById(R.id.outputDecimal);
        final EditText outputHexa = (EditText) v.findViewById(R.id.outputHexa);


        Button btn_decimal1 = (Button) v.findViewById(R.id.btn_decimal1);
        Button btn_binary1 = (Button) v.findViewById(R.id.btn_binary1);
        Button btn_octal1 = (Button) v.findViewById(R.id.btn_octal1);
        Button btn_hexa1 = (Button) v.findViewById(R.id.btn_hexa1);
        Button btn_decimal2 = (Button) v.findViewById(R.id.btn_decimal2);
        Button btn_binary2 = (Button) v.findViewById(R.id.btn_binary2);
        Button btn_octal2 = (Button) v.findViewById(R.id.btn_octal2);
        Button btn_hexa2 = (Button) v.findViewById(R.id.btn_hexa2);

        Button btn_add = (Button) v.findViewById(R.id.btn_add);
        Button btn_subtract = (Button) v.findViewById(R.id.btn_subtract);
        Button btn_clr = (Button) v.findViewById(R.id.btn_clr);


        final View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button btn = (Button) v;


                switch (btn.getId()) {

                    case R.id.btn_decimal1:
                        try {
                            decimal1 = Integer.parseInt(input1_decimal.getText().toString(), 10);

                            input1_binary.setText(Integer.toString(decimal1, 2));

                            input1_octal.setText(Integer.toString(decimal1, 8));

                            input1_hexa.setText(Integer.toString(decimal1, 16));

                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_decimal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();

                        }
                        break;


                    case R.id.btn_binary1:
                        try {


                            binary1 = Integer.parseInt(input1_binary.getText().toString(), 2);


                            input1_decimal.setText(Integer.toString(binary1, 10));

                            input1_octal.setText(Integer.toString(binary1, 8));

                            input1_hexa.setText(Integer.toString(binary1, 16));

                        } catch (NumberFormatException e) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_binary);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;


                    case R.id.btn_octal1:
                        try {
                            octal1 = Integer.parseInt(input1_octal.getText().toString(), 8);

                            input1_binary.setText(Integer.toString(octal1, 2));

                            input1_decimal.setText(Integer.toString(octal1, 10));

                            input1_hexa.setText(Integer.toString(octal1, 16));

                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_octal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();


                        }
                        break;

                    case R.id.btn_hexa1:

                        try {
                            hexa1 = Integer.parseInt(input1_hexa.getText().toString(), 16);

                            input1_binary.setText(Integer.toString(hexa1, 2));

                            input1_decimal.setText(Integer.toString(hexa1, 10));

                            input1_octal.setText(Integer.toString(hexa1, 8));

                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_hexadecimal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;

                    case R.id.btn_decimal2:
                        try {
                            decimal2 = Integer.parseInt(input2_decimal.getText().toString(), 10);

                            input2_binary.setText(Integer.toString(decimal2, 2));

                            input2_octal.setText(Integer.toString(decimal2, 8));

                            input2_hexa.setText(Integer.toString(decimal2, 16));
                        } catch (NumberFormatException e) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_decimal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;

                    case R.id.btn_binary2:
                        try {
                            binary2 = Integer.parseInt(input2_binary.getText().toString(), 2);

                            input2_decimal.setText(Integer.toString(binary2, 10));

                            input2_octal.setText(Integer.toString(binary2, 8));

                            input2_hexa.setText(Integer.toString(binary2, 16));

                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_binary);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;


                    case R.id.btn_octal2:
                        try {

                            octal2 = Integer.parseInt(input2_octal.getText().toString(), 8);

                            input2_binary.setText(Integer.toString(octal2, 2));

                            input2_decimal.setText(Integer.toString(octal2, 10));

                            input2_hexa.setText(Integer.toString(octal2, 16));

                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_octal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;

                    case R.id.btn_hexa2:
                        try {

                            hexa2 = Integer.parseInt(input2_hexa.getText().toString(), 16);

                            input2_binary.setText(Integer.toString(hexa2, 2));

                            input2_decimal.setText(Integer.toString(hexa2, 10));

                            input2_octal.setText(Integer.toString(hexa2, 8));
                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_hexadecimal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }
                        break;

                    case R.id.btn_add:


                        if ((decimal1 > 0) && (decimal2 > 0)) {

                            sum = decimal1 + decimal2;

                            outputBinary.setText(Integer.toString(sum, 2));

                            outputDecimal.setText(Integer.toString(sum, 10));

                            outputOctal.setText(Integer.toString(sum, 8));

                            outputHexa.setText(Integer.toString(sum, 16));

                        } else if ((binary1 > 0) && (binary2 > 0)) {
                            sum = binary1 + binary2;

                            outputBinary.setText(Integer.toString(sum, 2));

                            outputDecimal.setText(Integer.toString(sum, 10));

                            outputOctal.setText(Integer.toString(sum, 8));

                            outputHexa.setText(Integer.toString(sum, 16));

                        } else if ((octal1 > 0) && (octal2 > 0)) {
                            sum = octal1 + octal2;

                            outputBinary.setText(Integer.toString(sum, 2));

                            outputDecimal.setText(Integer.toString(sum, 10));

                            outputOctal.setText(Integer.toString(sum, 8));

                            outputHexa.setText(Integer.toString(sum, 16));

                        } else if ((hexa1 > 0) && (hexa2 > 0)) {
                            sum = hexa1 + hexa2;

                            outputBinary.setText(Integer.toString(sum, 2));

                            outputDecimal.setText(Integer.toString(sum, 10));

                            outputOctal.setText(Integer.toString(sum, 8));

                            outputHexa.setText(Integer.toString(sum, 16));
                        }


                        break;


                    case R.id.btn_subtract:


                        if (decimal1 > decimal2) {
                            difference = decimal1 - decimal2;

                            outputBinary.setText(Integer.toString(difference, 2));

                            outputDecimal.setText(Integer.toString(difference, 10));

                            outputOctal.setText(Integer.toString(difference, 8));

                            outputHexa.setText(Integer.toString(difference, 16));

                        } else if ((binary1 > 0) && (binary2 > 0)) {
                            difference = binary1 - binary2;

                            outputBinary.setText(Integer.toString(difference, 2));

                            outputDecimal.setText(Integer.toString(difference, 10));

                            outputOctal.setText(Integer.toString(difference, 8));

                            outputHexa.setText(Integer.toString(difference, 16));

                        } else if ((octal1 > 0) && (octal2 > 0)) {
                            difference = octal1 - octal2;

                            outputBinary.setText(Integer.toString(difference, 2));

                            outputDecimal.setText(Integer.toString(difference, 10));

                            outputOctal.setText(Integer.toString(difference, 8));

                            outputHexa.setText(Integer.toString(difference, 16));

                        } else if ((hexa1 > 0) && (hexa2 > 0)) {
                            difference = hexa1 - hexa2;

                            outputBinary.setText(Integer.toString(difference, 2));

                            outputDecimal.setText(Integer.toString(difference, 10));

                            outputOctal.setText(Integer.toString(difference, 8));

                            outputHexa.setText(Integer.toString(difference, 16));
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_subtract);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }


                        break;


                    case R.id.btn_clr:

                        input1_binary.getText().clear();
                        input1_decimal.getText().clear();
                        input1_hexa.getText().clear();
                        input1_octal.getText().clear();

                        input2_binary.getText().clear();
                        input2_decimal.getText().clear();
                        input2_hexa.getText().clear();
                        input2_octal.getText().clear();

                        outputBinary.getText().clear();
                        outputDecimal.getText().clear();
                        outputHexa.getText().clear();
                        outputOctal.getText().clear();

                        break;


                }


            }
        };

        btn_binary1.setOnClickListener(onClick);
        btn_octal1.setOnClickListener(onClick);
        btn_decimal1.setOnClickListener(onClick);
        btn_hexa1.setOnClickListener(onClick);

        btn_binary2.setOnClickListener(onClick);
        btn_octal2.setOnClickListener(onClick);
        btn_decimal2.setOnClickListener(onClick);
        btn_hexa2.setOnClickListener(onClick);

        btn_add.setOnClickListener(onClick);
        btn_clr.setOnClickListener(onClick);
        btn_subtract.setOnClickListener(onClick);

        return v;
    }

}
