package com.steven.frio.numberconverter;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class HexadecimalFragment extends Fragment {
    View v;

    public HexadecimalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_hexadecimal, container, false);


        final EditText editDecimal = (EditText) v.findViewById(R.id.editDecimal);
        final EditText editBinary = (EditText) v.findViewById(R.id.editBinary);
        final EditText editOctal = (EditText) v.findViewById(R.id.editOctal);
        final EditText editHexadecimal = (EditText) v.findViewById(R.id.editHexadecimal);

        Button btn_1 = (Button) v.findViewById(R.id.btn_1);
        Button btn_2 = (Button) v.findViewById(R.id.btn_2);
        Button btn_3 = (Button) v.findViewById(R.id.btn_3);
        Button btn_4 = (Button) v.findViewById(R.id.btn_4);
        Button btn_5 = (Button) v.findViewById(R.id.btn_5);
        Button btn_6 = (Button) v.findViewById(R.id.btn_6);
        Button btn_7 = (Button) v.findViewById(R.id.btn_7);
        Button btn_8 = (Button) v.findViewById(R.id.btn_8);
        Button btn_9 = (Button) v.findViewById(R.id.btn_9);
        Button btn_0 = (Button) v.findViewById(R.id.btn_0);
        Button btn_a = (Button) v.findViewById(R.id.btn_a);
        Button btn_b = (Button) v.findViewById(R.id.btn_b);
        Button btn_c = (Button) v.findViewById(R.id.btn_c);
        Button btn_d = (Button) v.findViewById(R.id.btn_d);
        Button btn_e = (Button) v.findViewById(R.id.btn_e);
        Button btn_f = (Button) v.findViewById(R.id.btn_f);
        Button btn_go = (Button) v.findViewById(R.id.btn_go);
        Button btn_clr = (Button) v.findViewById(R.id.btn_clr);


        View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button) v;

                switch (btn.getId()) {
                    case R.id.btn_1:
                        editHexadecimal.setText(editHexadecimal.getText() + "1");
                        break;
                    case R.id.btn_2:
                        editHexadecimal.setText(editHexadecimal.getText() + "2");
                        break;
                    case R.id.btn_3:
                        editHexadecimal.setText(editHexadecimal.getText() + "3");
                        break;
                    case R.id.btn_4:
                        editHexadecimal.setText(editHexadecimal.getText() + "4");
                        break;
                    case R.id.btn_5:
                        editHexadecimal.setText(editHexadecimal.getText() + "5");
                        break;
                    case R.id.btn_6:
                        editHexadecimal.setText(editHexadecimal.getText() + "6");
                        break;
                    case R.id.btn_7:
                        editHexadecimal.setText(editHexadecimal.getText() + "7");
                        break;
                    case R.id.btn_8:
                        editHexadecimal.setText(editHexadecimal.getText() + "8");
                        break;
                    case R.id.btn_9:
                        editHexadecimal.setText(editHexadecimal.getText() + "9");
                        break;
                    case R.id.btn_0:
                        editHexadecimal.setText(editHexadecimal.getText() + "0");
                        break;
                    case R.id.btn_a:
                        editHexadecimal.setText(editHexadecimal.getText() + "A");
                        break;
                    case R.id.btn_b:
                        editHexadecimal.setText(editHexadecimal.getText() + "B");
                        break;
                    case R.id.btn_c:
                        editHexadecimal.setText(editHexadecimal.getText() + "C");
                        break;
                    case R.id.btn_d:
                        editHexadecimal.setText(editHexadecimal.getText() + "D");
                        break;
                    case R.id.btn_e:
                        editHexadecimal.setText(editHexadecimal.getText() + "E");
                        break;
                    case R.id.btn_f:
                        editHexadecimal.setText(editHexadecimal.getText() + "F");
                        break;
                    case R.id.btn_clr:
                        editHexadecimal.setText("");
                        editBinary.setText("");
                        editOctal.setText("");
                        editDecimal.setText("");
                        break;
                    case R.id.btn_go:
                        try {
                            int hexadecimal = Integer.parseInt(editHexadecimal.getText().toString(), 16);

                            editBinary.setText(Integer.toString(hexadecimal, 2));

                            editDecimal.setText(Integer.toString(hexadecimal, 10));

                            editOctal.setText(Integer.toString(hexadecimal, 8));
                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_hexadecimal);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();
                        }


                        break;

                }
            }
        };

        btn_1.setOnClickListener(onClick);
        btn_2.setOnClickListener(onClick);
        btn_3.setOnClickListener(onClick);
        btn_4.setOnClickListener(onClick);
        btn_5.setOnClickListener(onClick);
        btn_6.setOnClickListener(onClick);
        btn_7.setOnClickListener(onClick);
        btn_8.setOnClickListener(onClick);
        btn_9.setOnClickListener(onClick);
        btn_0.setOnClickListener(onClick);
        btn_a.setOnClickListener(onClick);
        btn_b.setOnClickListener(onClick);
        btn_c.setOnClickListener(onClick);
        btn_d.setOnClickListener(onClick);
        btn_e.setOnClickListener(onClick);
        btn_f.setOnClickListener(onClick);
        btn_go.setOnClickListener(onClick);
        btn_clr.setOnClickListener(onClick);
        return v;
    }

}
