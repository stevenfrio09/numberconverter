package com.steven.frio.numberconverter;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class BinaryFragment extends Fragment {
    View v; // declare v as view variable - s

    public BinaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_binary, container, false);

        final EditText editBinary = (EditText) v.findViewById(R.id.editBinary); // declare id's of widgets - s
        final EditText editOctal = (EditText) v.findViewById(R.id.editOctal);
        final EditText editDecimal = (EditText) v.findViewById(R.id.editDecimal);
        final EditText editHexadecimal = (EditText) v.findViewById(R.id.editHexadecimal);

        Button btn_1 = (Button) v.findViewById(R.id.btn_1);
        Button btn_0 = (Button) v.findViewById(R.id.btn_0);
        Button btn_clr = (Button) v.findViewById(R.id.btn_clr);
        Button btn_go = (Button) v.findViewById(R.id.btn_go);

        final View.OnClickListener onClick = new View.OnClickListener() { //declare View.OnClickListener for multiple buttons using onclick - s
            @Override
            public void onClick(View v) {

                Button btn = (Button) v; // declare btn for further use - s


                switch (btn.getId()) { // switch statement obviously -s

                    case R.id.btn_1:
                        editBinary.setText(editBinary.getText() + "1");
                        break;

                    case R.id.btn_0:
                        editBinary.setText(editBinary.getText() + "0");
                        break;

                    case R.id.btn_clr:
                        editBinary.setText("");
                        editOctal.setText("");
                        editDecimal.setText("");
                        editHexadecimal.setText("");
                        break;

                    case R.id.btn_go:
                        try {
                            int binary = Integer.parseInt(editBinary.getText().toString(), 2);

                            editOctal.setText(Integer.toString(binary, 8));

                            editDecimal.setText(Integer.toString(binary, 10));

                            editHexadecimal.setText(Integer.toString(binary, 16));
                        } catch (NumberFormatException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.error_title);
                            builder.setMessage(R.string.error_binary);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog alertalert = builder.create();
                            alertalert.show();

                        }
                        break;
                }

            }
        };

        btn_0.setOnClickListener(onClick); // by google xD -s
        btn_1.setOnClickListener(onClick);
        btn_clr.setOnClickListener(onClick);
        btn_go.setOnClickListener(onClick);


        return v; // return the view
    }

}
